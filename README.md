# README #
Wowcat Mobile Code Challenge:
The Abacus Mobile App

### Quick summary ###
this is a cross-platform (React Native App)[https://facebook.github.io/react-native/] with basic Abascus functionality.


### UI Features ###

* Floating action button which is handles Toolbar with Num Pads selection (Units, Tens, Hundreds)
* Logout
* Num Pad for Abasucs
* Login Screen
* Responsive


# Project Structure
* src
* src/app
* src/app/actions (**This directory should contain the actions for communcating with the Api's, Databases and etc**)
* src/app/reducers (**This directory contains models/reducers**)
* src/app/components
* src/app/storage (**Main Entry Storage configation, will handle offline first functionality**)
* src/app/theme
* src/app/layouts
* src/app/providers
* src/app/internalization
* src/app.js (**Entrypoint**)
* config (**Mock User, testEnvironment configuration file**)


# Local installation / dev

* HomeBrew is package manager for MacOS
```sh
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```
* NodeJS is required to run and compile the app
```sh
$ brew install node
$ brew install watchman
```

* React Native CLI
```sh
$ npm install -g react-native-cli
```

# Git Clone && install
* [https://bitbucket.org/aklesky/wowcat-mobile-challenge](https://bitbucket.org/aklesky/wowcat-mobile-challenge)
```sh
$ git clone https://bitbucket.org/aklesky/wowcat-mobile-challenge
$ cd wowcat-mobile-challenge
$ npm i
```


# iOS: XCode should be installed from App Store
* [And Command Line Tools should be enabled - Please Follow The Link To Check](https://facebook.github.io/react-native/docs/getting-started.html#command-line-tools)

# Run IOS
```sh
$ react-native run-ios
```

# Android: Android Studio Should be installed
[Documentation](https://facebook.github.io/react-native/docs/getting-started.html#1-install-android-studio)
* Android SDK is required
* Android SDK Platform is requred
* Performance (Intel ® HAXM) is required
* Android Virtual Device is required

# Installing Android SDK
* [Documentation](https://facebook.github.io/react-native/docs/getting-started.html#2-install-the-android-sdk)
* [How To configure $ANDROID_HOME environment](https://facebook.github.io/react-native/docs/getting-started.html#3-configure-the-android-home-environment-variable)
* [How To add the android emulator](https://facebook.github.io/react-native/docs/getting-started.html#using-a-virtual-device)

# Run Android (it could be an emulator of physical device)
```sh
$ react-native run-android
```

### NOT INCLUDED ###
* manifest.json
* apple icons, android icons
* splash screen
* UI simple tests are added just for the demo

### Continuous Integration and Delivery ###
* Circle CI ( eslint test and npm test)
