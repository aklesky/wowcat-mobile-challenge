import { AsyncStorage } from 'react-native';
import { applyMiddleware, compose, createStore } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import logger from 'redux-logger';
import { destroy } from 'redux-form';

import { appIsMounted } from '../actions';
import reducers from '../reducers';

export default (initialState = {}) => {
  const middleware = [];

  if (__DEV__ === true) {
    middleware.push(logger);
  }

  const enhancers = [autoRehydrate()];
  const composeEnhancers = compose;

  const store = createStore(
    reducers,
    initialState,
    composeEnhancers(applyMiddleware(...middleware), ...enhancers),
  );

  persistStore(
    store,
    {
      storage: AsyncStorage,
      keyPrefix: 'wowcat:',
      blacklist: [
        'app',
      ],
    },
    () => {
      store.dispatch(appIsMounted(true));
      store.dispatch(destroy('login-form'));
    },
  );
  return store;
};
