import normalize from './normalize';

const type = {
  base: 'Roboto',
  bold: 'Roboto',
  emphasis: 'Roboto',
};

const size = {
  h1: normalize(38),
  h2: normalize(34),
  h3: normalize(30),
  h4: normalize(26),
  h5: normalize(20),
  h6: normalize(19),
  title: normalize(23),
  input: normalize(18),
  regular: normalize(17),
  medium: normalize(14),
  avg: normalize(13),
  small: normalize(12),
  tiny: normalize(8.5),
};

const transform = {
  uppercase: 'uppercase',
  lowercase: 'lowercase',
};

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2,
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3,
  },
  h4: {
    fontFamily: type.base,
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
  },
  normal: {
    fontFamily: type.base,
    fontSize: size.regular,
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
  },
};
module.exports = {
  type,
  size,
  style,
  transform,
};
