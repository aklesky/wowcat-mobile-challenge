import {
  StyleSheet,
} from 'react-native';

import { scrollViewContainer, viewContainer } from './containers';
import metrics from './metrics';
import { overlay, transparent } from './colors';
import normalize from './normalize';

module.exports = StyleSheet.create({
  scrollView: {
    ...scrollViewContainer,
  },
  container: {
    ...viewContainer,
    flexDirection: 'column',
  },
  contentContainer: {
    ...viewContainer,
    ...metrics.contentCenter,
  },
  overlayContainer: {
    ...StyleSheet.absoluteFillObject,
    flex: 1,
    zIndex: 1000,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: overlay,
  },
  clearHorizontalMargins: {
    marginLeft: 0,
    marginRight: 0,
    paddingLeft: 0,
    paddingRight: 0,
  },
  transparentSmallButtons: {
    width: normalize(77),
    backgroundColor: transparent,
  },
});
