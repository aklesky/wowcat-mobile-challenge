import { green500, orange500 } from './colors';

const uiTheme = {
  palette: {
    primaryColor: green500,
    accentColor: orange500,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
};

export default uiTheme;
