const viewContainer = {
  flex: 1,
};

const scrollViewContainer = {
  ...viewContainer,
};


module.exports = {
  viewContainer,
  scrollViewContainer,
};
