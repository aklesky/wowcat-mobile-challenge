
import uiTheme from './uiTheme';
import theme from './theme';
import normalize from './normalize';
import fonts from './fonts';
import metrics from './metrics';
import * as colors from './colors';
import images from './images';
import dimensions from './dimensions';

module.exports = {
  uiTheme,
  theme,
  normalize,
  fonts,
  metrics,
  colors,
  images,
  dimensions,
};
