import React from 'react';
import { AppRouter } from './router';

import createStore from './storage/configureStore';
import AppProvider from './providers/AppProvider';
import ThemeProvider from './providers/ThemeProvider';

const INITIAL_STATE = {};

const store = createStore(INITIAL_STATE);

export default () => (
  <AppProvider store={store}>
    <ThemeProvider>
      <AppRouter />
    </ThemeProvider>
  </AppProvider>
);
