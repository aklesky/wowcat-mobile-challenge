import locale from '../locales/en';

describe('Testing locale strings', () => {
  it('should display Login', () => {
    expect(locale.login).toBe('Login');
  });
});
