import I18n from 'react-native-i18n';
import en from './locales/en';

I18n.fallbacks = true;

I18n.translations = {
  en,
};

export default key => I18n.t(key);
