import { App } from '../reducers';
import { appIsMounted } from './app';

describe('App Actions tests', () => {
  it('should put the state of the app as rehydrated', () => {
    const expectedAction = {
      type: App.UPDATE,
      payload: {
        restored: true,
      },
    };
    expect(appIsMounted(true)).toEqual(expectedAction);
  });
});
