
import { SubmissionError } from 'redux-form';
import { User } from '../reducers';
import mockUser from '../../config/user.mock';
import locale from '../internalization';

export const doAuthenticate = (values, dispatch) => {
  if (values.email !== mockUser.email || values.password !== mockUser.password) {
    throw new SubmissionError({
      _error: locale('incorrectUsername'),
    });
  }

  // mocking server response

  return new Promise((resolve) => {
    setTimeout(() => {
      dispatch({
        type: User.UPDATE,
        payload: {
          authenticated: true,
          username: values.email,
        },
      });
      resolve(true);
    }, 1000);
  });
};


export const doLogout = () => ({
  type: User.UPDATE,
  payload: {
    authenticated: false,
  },
});
