import { App, User } from '../reducers';

export const appIsMounted = (state = false) => ({
  type: App.UPDATE,
  payload: {
    restored: state,
  },
});

export const onClearAmount = () => ({
  type: User.UPDATE,
  payload: {
    amount: 0,
  },
});

export const onAddAmount = amount => ({
  type: User.UPDATE,
  payload: {
    amount,
  },
});
