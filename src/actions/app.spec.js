import { App } from '../reducers';
import { appIsMounted, onClearAmount, onAddAmount } from './app';
import reducer from '../reducers/user';

describe('App Actions tests', () => {
  it('should put the state of the app as rehydrated', () => {
    const expectedAction = {
      type: App.UPDATE,
      payload: {
        restored: true,
      },
    };
    expect(appIsMounted(true)).toEqual(expectedAction);
  });
  it('should add amount to the current', () => {
    const state = reducer(undefined, onAddAmount(781));
    expect(state).toEqual({
      amount: 781,
      authenticated: false,
      username: null,
    });
  });

  it('should clear the amount', () => {
    const state = reducer(undefined, onAddAmount(900));
    expect(reducer(state, onClearAmount())).toEqual({
      authenticated: false,
      username: null,
      amount: 0,
    });
  });
});
