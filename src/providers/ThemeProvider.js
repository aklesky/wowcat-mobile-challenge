import React from 'react';
import PropTypes from 'prop-types';

import { ThemeProvider as Theme } from 'react-native-material-ui';

import { uiTheme } from '../theme';


const ThemeProvider = ({ children }) => (<Theme uiTheme={uiTheme}>{children}</Theme>);

ThemeProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired,
};

export default ThemeProvider;
