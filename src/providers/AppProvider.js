import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';

const AppProvider = ({ children, store }) => (
  <Provider store={store}>{children}</Provider>
);

AppProvider.propTypes = {
  store: PropTypes.shape({
    dispatch: PropTypes.func.isRequired,
    getState: PropTypes.func.isRequired,
    replaceReducer: PropTypes.func.isRequired,
    subscribe: PropTypes.func.isRequired,
  }).isRequired,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired,
};

export default AppProvider;
