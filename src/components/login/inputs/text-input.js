import React from 'react';
import PropTypes from 'prop-types';
import { FormLabel, FormInput, FormValidationMessage } from 'react-native-elements';
import {
  View,
} from 'react-native';
import styles from '../styles/form';


const TextInput = ({
  input: { value, onChange, ...restInput },
  label,
  secure,
  meta: { touched, error },
}) => (
  <View>
    <FormLabel labelStyle={styles.labelStyle}>{label}</FormLabel>
    <FormInput
      inputStyle={styles.inputStyle}
      autoCapitalize={'none'}
      placeholder={label}
      onChangeText={onChange}
      secureTextEntry={secure}
      value={value}
      shake={!!(error)}
      {...restInput}
    />
    {touched && error && (<FormValidationMessage>{error}</FormValidationMessage>)}
  </View>
);


TextInput.defaultProps = {
  secure: false,
};

TextInput.propTypes = {
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string,
  }).isRequired,
  label: PropTypes.string.isRequired,
  secure: PropTypes.bool,
  meta: PropTypes.shape({
    error: PropTypes.string,
  }).isRequired,
};

export default TextInput;

