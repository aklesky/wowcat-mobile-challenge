import locale from '../../internalization';

const validate = (values) => {
  const errors = {};
  if (!values.email || values.email.trim() === '') {
    errors.email = locale('required');
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email.trim())) {
    errors.email = locale('incorrectEmail');
  }
  if (!values.password || values.password.trim() === '') {
    errors.password = locale('required');
  } else if (!/^(?=.*[A-Za-z])(?=.*\d)(?=.*[$@$!%*#?&])[A-Za-z\d$@$!%*#?&]{10,}$/ig.test(values.password.trim())) {
    errors.password = locale('incorrectPassword');
  }

  return errors;
};

export default validate;
