import validate from '../validations';
import locale from '../../../internalization';


describe('Tests for validate inputs', () => {
  const required = locale('required');
  const incorrectEmail = locale('incorrectEmail');
  const incorrectPassword = locale('incorrectPassword');

  it('should handle required email address', () => {
    let toValidate = validate({
      email: null,
    });
    expect(toValidate.email).toEqual(required);

    toValidate = validate({
      email: ' ',
    });
    expect(toValidate.email).toEqual(required);
  });

  it('should handle invalid email address', () => {
    const values = {
      email: 'test.test',
    };
    const toValidate = validate(values);
    expect(toValidate.email).toEqual(incorrectEmail);
  });

  it('should handle correct email validation', () => {
    const values = {
      email: 'example@example.com',
    };
    const toValidate = validate(values);
    expect(toValidate.email).toBeFalsy();
  });

  it('should handle require password', () => {
    let toValidate = validate({
      password: null,
    });
    expect(toValidate.password).toEqual(required);

    toValidate = validate({
      password: ' ',
    });
    expect(toValidate.password).toEqual(required);
  });

  it('should handle password validation', () => {
    let toValidate = validate({
      password: '123456',
    });
    expect(toValidate.password).toEqual(incorrectPassword);

    toValidate = validate({
      password: '12345678aA',
    });
    expect(toValidate.password).toEqual(incorrectPassword);
  });

  it('should handle correct password validation', () => {
    const values = {
      password: '12345678a$A',
    };
    const toValidate = validate(values);
    expect(toValidate.password).toBeFalsy();
  });
});
