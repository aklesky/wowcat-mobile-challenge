import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
} from 'react-native';
import { reduxForm, Field } from 'redux-form';

import { Button, FormLabel } from 'react-native-elements';
import I18n from '../../internalization';
import { TextField } from './inputs';
import validate from './validations';
import { doAuthenticate } from '../../actions';
import styles from './styles/form';
import { uiTheme } from '../../theme';

const LoginForm = (props) => {
  const { handleSubmit, pristine, submitting, error } = props;
  return (
    <View>
      {error && (<FormLabel>{error}</FormLabel>)}
      <Field name={'email'} label={I18n('email')} component={TextField} />
      <Field name={'password'} label={I18n('password')} secure component={TextField} />
      <Button
        containerViewStyle={styles.buttonContainerStyles}
        onPress={handleSubmit(doAuthenticate)}
        raised
        primary
        title={I18n('login')}
        backgroundColor={uiTheme.palette.primaryColor}
        disabled={pristine || submitting}
      />
    </View>
  );
};

LoginForm.defaultProps = {
  error: null,
};

LoginForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  pristine: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

export default reduxForm({
  form: 'login-form',
  validate,
})(LoginForm);
