import { metrics, colors } from '../../../theme';

const styles = {
  buttonContainerStyles: {
    marginTop: metrics.baseMargin,
  },
  labelStyle: {
    color: colors.green500,
  },
  inputStyle: {
    color: colors.blueGrey700,
  },
};

module.exports = styles;
