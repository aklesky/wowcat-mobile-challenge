import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
} from 'react-native';
import { Dialog } from 'react-native-material-ui';

import styles from './styles';

const DialogView = props => (
  <Dialog>
    <Dialog.Title><Text>{props.title}</Text></Dialog.Title>
    <Dialog.Content>
      <Text>{props.content}</Text>
    </Dialog.Content>
    <Dialog.Actions>
      <View style={styles}>
        {props.actions}
      </View>
    </Dialog.Actions>
  </Dialog>);


DialogView.propTypes = {
  content: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  actions: PropTypes.arrayOf(PropTypes.element).isRequired,
};
export default DialogView;
