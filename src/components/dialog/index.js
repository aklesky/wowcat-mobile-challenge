import DialogAction from './Dialog.Action';
import Dialog from './Dialog';

module.exports = {
  Dialog,
  DialogAction,
};
