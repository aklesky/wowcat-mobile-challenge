import { metrics } from '../../theme';

export default {
  flexDirection: 'row',
  alignItems: 'stretch',
  justifyContent: 'flex-end',
  paddingVertical: metrics.smallMargin,
};
