import React from 'react';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import { theme, uiTheme } from '../../theme';

const Action = props => (
  <Button
    containerViewStyle={theme.clearHorizontalMargins}
    buttonStyle={theme.transparentSmallButtons}
    color={uiTheme.palette.primaryColor}
    onPress={props.onPress}
    title={props.title}
  />
);

Action.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default Action;

