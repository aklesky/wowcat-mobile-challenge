import React from 'react';
import {
  View,
} from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'react-native-elements';
import { metrics } from '../../theme';
import styles from './styles';

const Pad = (props) => {
  const { numPads, onPress, onClear, multiply } = props;
  return (<View style={styles.numPadContainer}>
    {numPads.map((pad, i) => {
      const key = i * multiply;
      return (
        <View style={styles.keyPadContainer} key={key}>
          {pad.map((e) => {
            const num = e * multiply;
            return (
              <Button
                containerViewStyle={styles.borderRadius}
                buttonStyle={styles.buttonStyle}
                borderRadius={metrics.images.large / 2}
                key={num}
                title={num.toString()}
                onPress={() => onPress(num)}
              />
            );
          })}
        </View>
      );
    })}
    <View style={styles.keyPadContainer}>
      <Button
        onPress={onClear}
        containerViewStyle={styles.borderRadius}
        buttonStyle={styles.buttonStyle}
        borderRadius={metrics.images.large / 2}
        title="0"
      />
    </View>
  </View>);
};

Pad.propTypes = {
  numPads: PropTypes.arrayOf(PropTypes.array).isRequired,
  onClear: PropTypes.func.isRequired,
  onPress: PropTypes.func.isRequired,
  multiply: PropTypes.number.isRequired,
};

export default Pad;
