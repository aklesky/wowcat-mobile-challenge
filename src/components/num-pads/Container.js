import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { theme, metrics, colors } from '../../theme';
import locale from '../../internalization';
import ActionButton from '../action-button/ActionButton';
import NumPads from './NumPads';
import ActionUnit from '../action-button/ActionUnit';
import { onClearAmount, onAddAmount } from '../../actions';
import Dialog from '../dialog/Dialog';
import Action from '../dialog/Dialog.Action';

class NumPadContainer extends React.PureComponent {
  static propTypes = {
    onClearAmount: PropTypes.func.isRequired,
    onAddAmount: PropTypes.func.isRequired,
    amount: PropTypes.number.isRequired,
  }


  constructor(props) {
    super(props);
    this.state = {
      numpads: [[1, 2, 3], [4, 5, 6], [7, 8, 9]],
      multiply: 1,
      showDialog: false,
    };
    this.onPress = this.onPress.bind(this);
    this.onAdd = this.onAdd.bind(this);
    this.onClear = this.onClear.bind(this);
    this.closeDialog = this.closeDialog.bind(this);
    this.openDialog = this.openDialog.bind(this);
  }

  onPress(multiply) {
    this.setState({ multiply: 1 * multiply });
  }

  onClear() {
    this.props.onClearAmount();
    this.closeDialog();
    return this;
  }

  onAdd(num) {
    this.props.onAddAmount(this.props.amount + num);
    return this;
  }


  onShowDialog = () => {
    if (this.state.showDialog) {
      return (
        <View style={theme.overlayContainer}>
          <Dialog
            title={locale('confirm')}
            content={locale('clearMessage')}
            actions={[
              <Action key="cancel" onPress={this.closeDialog} title={locale('dismiss')} />,
              <Action key="confirm" onPress={this.onClear} title={locale('confirm')} />,
            ]}
          />
        </View>
      );
    }
    return null;
  };

  openDialog() {
    this.setState({ showDialog: true });
  }

  closeDialog() {
    this.setState({ showDialog: false });
  }


  render() {
    return (
      <View style={theme.container}>
        {this.onShowDialog()}
        <View
          style={[
            theme.container, theme.contentContainer,
            { marginTop: metrics.navBarHeight },
          ]}
        >
          <View style={[theme.container, metrics.contentCenter]}>
            <NumPads
              numPads={this.state.numpads}
              multiply={this.state.multiply}
              onPress={this.onAdd}
              onClear={this.openDialog}
            />
          </View>
          <ActionButton
            actions={[
              <ActionUnit key="unit" onPress={() => this.onPress(1)} label={locale('units')} />,
              <ActionUnit key="tens" onPress={() => this.onPress(10)} label={locale('tens')} />,
              <ActionUnit key="hundreds" onPress={() => this.onPress(100)} label={locale('hundreds')} />,
              <View><Icon name="close" size={metrics.icons.small} color={colors.white} /></View>,
            ]}
            transition="toolbar"
          />
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  amount: state.user.amount,
});

export default connect(mapStateToProps, {
  onClearAmount,
  onAddAmount,
})(NumPadContainer);
