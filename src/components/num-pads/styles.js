import { metrics, uiTheme } from '../../theme';

const styles = {
  container: {
    ...metrics.contentCenter,
  },
  borderRadius: {
    borderRadius: metrics.images.large / 2,
  },
  buttonStyle: {
    width: metrics.images.large,
    height: metrics.images.large,
    borderRadius: metrics.images.large / 2,
    backgroundColor: uiTheme.palette.accentColor,
  },
  keyPadContainer: {
    ...metrics.row,
    ...metrics.alignCenter,
    ...metrics.contentCenter,
    padding: metrics.smallMargin,
  },
  numPadContainer: { flex: 1, ...metrics.contentCenter },
};

export default styles;
