import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  View,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';

import {
  Header,
} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { doLogout } from '../../actions';

import locale from '../../internalization';
import styles from './styles';

class ToolBar extends React.PureComponent {
  static defaultProps = {
    amount: 0,
  }
  static propTypes = {
    username: PropTypes.string.isRequired,
    amount: PropTypes.number,
    doLogout: PropTypes.func.isRequired,
    onRightAction: PropTypes.func.isRequired,
  }

  render() {
    return (
      <View style={styles.zIndex}>
        <Header
          outerContainerStyles={styles.header}
          leftComponent={
            <View style={styles.leftContainer}>
              <Text style={styles.text}>{this.props.username}</Text>
            </View>
          }
          centerComponent={
            <View style={styles.leftContainer}>
              <Text style={styles.text}>{locale('amount')} {this.props.amount}</Text>
            </View>
          }
          rightComponent={
            <TouchableWithoutFeedback
              onPress={this.props.onRightAction}
            >
              <View style={styles.container}>
                <Icon {...styles.icon} />
              </View>
            </TouchableWithoutFeedback>
          }
        />
      </View>);
  }
}

const mapStateToProps = state => ({
  username: state.user.username,
  amount: state.user.amount,
});

export default connect(mapStateToProps, {
  doLogout,
})(ToolBar);

