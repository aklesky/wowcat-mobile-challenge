import { metrics, colors, fonts, uiTheme } from '../../theme';

export default {
  zIndex: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },
  leftContainer: {
    paddingHorizontal: metrics.smallMargin,
    paddingVertical: metrics.baseMargin,

  },
  container: {
    padding: metrics.baseMargin,
  },
  header: {
    height: metrics.navBarHeight,
    backgroundColor: uiTheme.palette.primaryColor,
  },
  text: {
    fontSize: fonts.size.small,
    color: colors.white,
  },
  icon: {
    name: 'exit-to-app',
    color: colors.white,
    size: fonts.size.regular,
  },
};
