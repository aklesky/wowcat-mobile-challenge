import React from 'react';
import {
  Platform,
  UIManager,
} from 'react-native';
import PropTypes from 'prop-types';
import { ActionButton } from 'react-native-material-ui';
import styles from './styles';

if (Platform.OS === 'android') {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const FAB = props => (
  <ActionButton
    style={styles.actionButtonContainer}
    actions={props.actions}
    icon={props.icon}
    transition={props.transition}
  />
);

FAB.defaultProps = {
  actions: [],
  icon: 'dialpad',
  transition: 'speedDial',
};

FAB.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.element),
  icon: PropTypes.string,
  transition: PropTypes.string,
};

export default FAB;
