
import {
  colors,
  metrics,
  fonts,
  uiTheme,
} from '../../theme';

export default {
  container: {
    ...metrics.alignCenter,
    marginTop: metrics.smallMargin,
  },
  icon: {
    size: metrics.icons.small,
    color: colors.white,
  },
  text: {
    marginTop: metrics.smallMargin,
    marginBottom: metrics.smallMargin,
    color: colors.white,
    fontSize: fonts.size.small,
  },
  actionButtonContainer: {
    container: {
      backgroundColor: uiTheme.palette.accentColor,
    },
    toolbarContainer: {
      backgroundColor: uiTheme.palette.primaryColor,
    },
  },
};
