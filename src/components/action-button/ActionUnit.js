import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  TouchableWithoutFeedback,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';

import styles from './styles';

const Unit = props => (
  <TouchableWithoutFeedback onPress={props.onPress}>
    <View style={styles.container}>
      <Icon name={props.icon} {...styles.icon} /><Text style={styles.text}>{props.label}</Text>
    </View>
  </TouchableWithoutFeedback>
);

Unit.defaultProps = {
  label: null,
  icon: 'dialpad',
};

Unit.propTypes = {
  onPress: PropTypes.func.isRequired,
  label: PropTypes.string,
  icon: PropTypes.string,
};

export default Unit;
