import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import rootNavigator from './navigators/root';

class AppRouter extends React.Component {
  static defaultProps = {
    authenticated: false,
    restored: false,
  }

  static propTypes = {
    authenticated: PropTypes.bool.isRequired,
    restored: PropTypes.bool.isRequired,
  }

  shouldComponentUpdate(newProps) {
    if (newProps.restored !== this.props.restored) {
      return true;
    }
    if (newProps.authenticated !== this.props.authenticated) {
      return true;
    }
    return false;
  }

  render() {
    if (!this.props.restored) {
      return null;
    }
    const Router = rootNavigator(this.props.authenticated);
    return (<Router />);
  }
}

const mapStateToProps = (state) => {
  const { restored } = state.app;
  const { authenticated } = state.user;
  return {
    restored,
    authenticated,
  };
};

export default connect(mapStateToProps, null)(AppRouter);
