import { StackNavigator } from 'react-navigation';

import { Login, Home } from '../../layouts';

const rootNavigator = (authenticationState = false) => (
  StackNavigator({
    LoginScreen: {
      screen: Login,
    },
    HomeScreen: {
      screen: Home,
    },
  },
  {
    headerMode: 'none',
    initialRouteName: authenticationState ? 'HomeScreen' : 'LoginScreen',
  })
);

export default rootNavigator;
