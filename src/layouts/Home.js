import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
} from 'react-native';
import { connect } from 'react-redux';
import { doLogout } from '../actions';
import { theme } from '../theme';
import locale from '../internalization';

import { ToolBar, Dialog, DialogAction, NumPad } from '../components';


class Home extends React.PureComponent {
  static propTypes = {
    doLogout: PropTypes.func.isRequired,
  }
  constructor(props) {
    super(props);
    this.state = {
      showDialog: false,
    };
    this.closeDialog = this.closeDialog.bind(this);
    this.onLogout = this.onLogout.bind(this);
    this.openDialog = this.openDialog.bind(this);
  }

  onShowDialog = () => {
    if (this.state.showDialog) {
      return (
        <View style={theme.overlayContainer}>
          <Dialog
            title={locale('confirm')}
            content={locale('logoutMessage')}
            actions={[
              <DialogAction key="cancel" onPress={this.closeDialog} title={locale('dismiss')} />,
              <DialogAction key="confirm" onPress={this.onLogout} title={locale('logout')} />,
            ]}
          />
        </View>
      );
    }
    return null;
  };

  onLogout() {
    this.closeDialog();
    this.props.doLogout();
  }

  openDialog() {
    this.setState({ showDialog: true });
  }

  closeDialog() {
    this.setState({ showDialog: false });
  }

  render() {
    return (<View style={theme.container}>
      {this.onShowDialog()}
      <ToolBar onRightAction={this.openDialog} />
      <NumPad />
    </View>);
  }
}

export default connect(null, {
  doLogout,
})(Home);
