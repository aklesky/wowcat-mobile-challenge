import React from 'react';
import {
  ImageBackground,
} from 'react-native';

import { Card } from 'react-native-elements';
import LoginForm from '../components/login/form';

import { images } from '../theme';
import styles from './styles/login';

export default () => (
  <ImageBackground source={images.background} style={styles.container}>
    <Card containerStyle={styles.cardContainer} wrapperStyle={styles.cardInnerStyle}>
      <LoginForm />
    </Card>
  </ImageBackground>

);
