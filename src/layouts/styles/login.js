import {
  StyleSheet,
} from 'react-native';

import { metrics, colors } from '../../theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null,
    ...metrics.contentCenter,
    ...metrics.alignCenter,
  },
  cardContainer: {
    ...metrics.contentCenter,
    ...metrics.alignCenter,

    backgroundColor: colors.whiteOverlay,
    borderWidth: 0,
  },
  cardInnerStyle: {
    paddingLeft: metrics.baseMargin,
    marginLeft: metrics.baseMargin,
    paddingRight: metrics.baseMargin,
    borderWidth: 0,
  },
});
