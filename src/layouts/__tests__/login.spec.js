import React from 'react';
import chai, { expect } from 'chai';
import sinonChai from 'sinon-chai';
import { shallow } from 'enzyme';
import {
  ImageBackground,
} from 'react-native';
import {
  Card,
} from 'react-native-elements';

import { LoginForm } from '../../components';

import Login from '../Login';

chai.use(sinonChai);


describe('App should render', () => {
  it('should render the login screen', () => {
    const shadow = shallow(<Login />);
    expect(shadow.find(LoginForm).length).to.be.equal(1);
  });

  it('should find ImageBackground on the login screen', () => {
    const shadow = shallow(<Login />);
    const imageBackground = shadow.find(ImageBackground);
    expect(imageBackground.length).to.be.equal(1);
  });
  it('should find Card on the login screen', () => {
    const shadow = shallow(<Login />);
    const card = shadow.find(Card);
    expect(card.length).to.be.equal(1);
  });
});
