import reducer, { actionTypes } from './app';

describe('App reducer test', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        restored: false,
      },
    );
  });

  it('authenticated state should be updated to true', () => {
    expect(reducer(undefined, {
      type: actionTypes.UPDATE,
      payload: {
        restored: false,
      },
    })).toEqual({
      restored: false,
    });
  });

  it('should handle reset state', () => {
    const state = reducer(undefined, {
      type: actionTypes.UPDATE,
      payload: {
        restored: true,
      },
    });
    expect(reducer(state, {
      type: actionTypes.RESET,
    })).toEqual({
      restored: false,
    });
  });
});
