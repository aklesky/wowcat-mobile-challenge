const NAMESPACE = '/app/user';

const RESET = `${NAMESPACE}/reset`;
const UPDATE = `${NAMESPACE}/update`;

const initialState = {
  authenticated: false,
  amount: 0,
  username: null,
};

const actionTypes = {
  UPDATE,
  RESET,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE:
      return {
        ...state,
        ...action.payload,
      };
    case RESET:
      return initialState;
    default:
      return state;
  }
};

export {
  actionTypes,
};

export default reducer;
