import {
  combineReducers,
} from 'redux';

import { reducer as form } from 'redux-form';

import initial, {
  actionTypes as Initial,
} from './initial';
import app, {
  actionTypes as App,
} from './app';

import user, {
  actionTypes as User,
} from './user';


export {
  Initial,
  App,
  User,
};

export default combineReducers({
  initial,
  app,
  user,
  form,
});
