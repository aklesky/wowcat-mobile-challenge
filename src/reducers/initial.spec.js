import reducer, { actionTypes } from './initial';

describe('Initial reducer test', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        dispatched: false,
      },
    );
  });

  it('dispatched state should be updated to true', () => {
    expect(reducer(undefined, {
      type: actionTypes.UPDATE,
      payload: {
        dispatched: true,
      },
    })).toEqual({
      dispatched: true,
    });
  });

  it('should handle reset state', () => {
    const state = reducer(undefined, {
      type: actionTypes.UPDATE,
      payload: {
        dispatched: true,
      },
    });
    expect(reducer(state, {
      type: actionTypes.RESET,
    })).toEqual({
      dispatched: false,
    });
  });
});
