const NAMESPACE = '/app/initial';

const RESET = `${NAMESPACE}/reset`;
const UPDATE = `${NAMESPACE}/update`;

const initialState = {
  dispatched: false,
};

const actionTypes = {
  UPDATE,
  RESET,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE:
      return {
        ...state,
        ...action.payload,
      };
    case RESET:
      return initialState;
    default:
      return state;
  }
};

export {
  actionTypes,
};

export default reducer;
