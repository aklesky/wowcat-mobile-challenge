import React from 'react';
import chai, { expect } from 'chai';
import sinonChai from 'sinon-chai';
import { shallow } from 'enzyme';
import AppProvider from '../src/providers/AppProvider';
import App from '../src/app';

chai.use(sinonChai);


describe('App should render', () => {
  it('should render the app', () => {
    const shadow = shallow(<App />);
    expect(shadow.find(AppProvider).length).to.be.equal(1);
  });
});
