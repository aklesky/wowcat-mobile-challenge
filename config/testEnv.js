
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

__DEV__ = false;

console.error = () => {};

jest.mock('react-native-i18n', () => {
  const i18njs = require('i18n-js');
  const en = require('../src/internalization/locales/en');
  i18njs.translations = { en };

  return {
    t: jest.fn((k, o) => i18njs.t(k, o)),
  };
});

Enzyme.configure({ adapter: new Adapter() });
